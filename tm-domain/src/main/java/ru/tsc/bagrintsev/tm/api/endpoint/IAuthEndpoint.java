package ru.tsc.bagrintsev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignInRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignOutRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserViewProfileRequest;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignInResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignOutResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserViewProfileResponse;

public interface IAuthEndpoint extends IAbstractEndpoint {

    @NotNull
    UserSignInResponse signIn(@NotNull final UserSignInRequest request);

    @NotNull
    UserSignOutResponse signOut(@NotNull final UserSignOutRequest request);

    @NotNull
    UserViewProfileResponse viewProfile(@NotNull final UserViewProfileRequest request);

}
