package ru.tsc.bagrintsev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.data.*;
import ru.tsc.bagrintsev.tm.dto.response.data.*;

public interface IDomainEndpoint extends IAbstractEndpoint {

    @NotNull
    BackupLoadResponse loadBackup(@NotNull final BackupLoadRequest request);

    @NotNull
    BackupSaveResponse saveBackup(@NotNull final BackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse loadBase64(@NotNull final DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse saveBase64(@NotNull final DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse loadBinary(@NotNull final DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse saveBinary(@NotNull final DataBinarySaveRequest request);

    @NotNull
    DataJacksonJsonLoadResponse loadJacksonJson(@NotNull final DataJacksonJsonLoadRequest request);

    @NotNull
    DataJacksonJsonSaveResponse saveJacksonJson(@NotNull final DataJacksonJsonSaveRequest request);

    @NotNull
    DataJacksonXmlLoadResponse loadJacksonXml(@NotNull final DataJacksonXmlLoadRequest request);

    @NotNull
    DataJacksonXmlSaveResponse saveJacksonXml(@NotNull final DataJacksonXmlSaveRequest request);

    @NotNull
    DataJacksonYamlLoadResponse loadJacksonYaml(@NotNull final DataJacksonYamlLoadRequest request);

    @NotNull
    DataJacksonYamlSaveResponse saveJacksonYaml(@NotNull final DataJacksonYamlSaveRequest request);

    @NotNull
    DataJaxbJsonLoadResponse loadJaxbJson(@NotNull final DataJaxbJsonLoadRequest request);

    @NotNull
    DataJaxbJsonSaveResponse saveJaxbJson(@NotNull final DataJaxbJsonSaveRequest request);

    @NotNull
    DataJaxbXmlLoadResponse loadJaxbXml(@NotNull final DataJaxbXmlLoadRequest request);

    @NotNull
    DataJaxbXmlSaveResponse saveJaxbXml(@NotNull final DataJaxbXmlSaveRequest request);

}
