package ru.tsc.bagrintsev.tm.dto.request.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractRequest;
import ru.tsc.bagrintsev.tm.enumerated.Role;

@Getter
@Setter
@AllArgsConstructor
public final class UserSignUpRequest extends AbstractRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private Role role;

    @Nullable
    private String email;

}
