package ru.tsc.bagrintsev.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@AllArgsConstructor
public final class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private String statusValue;

}
