package ru.tsc.bagrintsev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.user.*;
import ru.tsc.bagrintsev.tm.dto.response.user.*;

public interface IUserEndpoint extends IAbstractEndpoint {

    @NotNull
    UserChangePasswordResponse changePassword(@NotNull final UserChangePasswordRequest request);

    @NotNull
    UserLockResponse lock(@NotNull final UserLockRequest request);

    @NotNull
    UserRemoveResponse remove(@NotNull final UserRemoveRequest request);

    @NotNull
    UserSignUpResponse signUp(@NotNull final UserSignUpRequest request);

    @NotNull
    UserUnlockResponse unlock(@NotNull final UserUnlockRequest request);

    @NotNull
    UserUpdateProfileResponse updateProfile(@NotNull final UserUpdateProfileRequest request);

}
