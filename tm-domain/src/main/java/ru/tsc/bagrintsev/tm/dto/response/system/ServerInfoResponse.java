package ru.tsc.bagrintsev.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class ServerInfoResponse extends AbstractResponse {

    @NotNull
    String processors;

    @NotNull
    String freeMemory;

    @NotNull
    String maxMemory;

    @NotNull
    String totalMemory;

    @NotNull
    String usedMemory;

}
