package ru.tsc.bagrintsev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.system.AboutRequest;
import ru.tsc.bagrintsev.tm.dto.request.system.InfoRequest;
import ru.tsc.bagrintsev.tm.dto.request.system.VersionRequest;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerAboutResponse;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerInfoResponse;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerVersionResponse;

public interface ISystemEndpoint extends IAbstractEndpoint {
    @NotNull
    ServerAboutResponse getAbout(@NotNull final AboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull final VersionRequest request);

    @NotNull
    ServerInfoResponse getInfo(@NotNull final InfoRequest request);

}
