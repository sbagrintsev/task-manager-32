package ru.tsc.bagrintsev.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.model.Task;

@NoArgsConstructor
public final class TaskChangeStatusByIdResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIdResponse(@Nullable Task task) {
        super(task);
    }

}
