package ru.tsc.bagrintsev.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.response.AbstractResponse;
import ru.tsc.bagrintsev.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable
    private Project project;

    public AbstractProjectResponse(@Nullable Project project) {
        this.project = project;
    }

}
