package ru.tsc.bagrintsev.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;

@Getter
@NoArgsConstructor
public final class TaskListResponse extends AbstractTaskResponse {

    @Nullable
    List<Task> tasks;

    public TaskListResponse(@Nullable List<Task> tasks) {
        this.tasks = tasks;
    }

}
