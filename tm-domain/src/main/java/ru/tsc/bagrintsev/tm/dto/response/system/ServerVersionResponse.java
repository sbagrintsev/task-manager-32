package ru.tsc.bagrintsev.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class ServerVersionResponse extends AbstractResponse {

    @NotNull
    private String version;

}
