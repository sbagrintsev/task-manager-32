package ru.tsc.bagrintsev.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class UserSignInResponse extends AbstractResultResponse {

    public UserSignInResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
