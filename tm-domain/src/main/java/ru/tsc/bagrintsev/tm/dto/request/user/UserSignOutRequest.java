package ru.tsc.bagrintsev.tm.dto.request.user;

import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

public final class UserSignOutRequest extends AbstractUserRequest {
}
