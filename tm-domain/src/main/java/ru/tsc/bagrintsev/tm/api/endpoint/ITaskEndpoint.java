package ru.tsc.bagrintsev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.task.*;
import ru.tsc.bagrintsev.tm.dto.response.task.*;

public interface ITaskEndpoint extends IAbstractEndpoint {

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskClearResponse clearTask(@NotNull TaskClearRequest request);

    @NotNull
    TaskCreateResponse createTask(@NotNull TaskCreateRequest request);

    @NotNull
    TaskListByProjectIdResponse listTaskByProjectId(@NotNull TaskListByProjectIdRequest request);

    @NotNull
    TaskListResponse listTask(@NotNull TaskListRequest request);

    @NotNull
    TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    TaskShowByIdResponse showTaskById(@NotNull TaskShowByIdRequest request);

    @NotNull
    TaskShowByIndexResponse showTaskByIndex(@NotNull TaskShowByIndexRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request);

    @NotNull
    TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request);

}
