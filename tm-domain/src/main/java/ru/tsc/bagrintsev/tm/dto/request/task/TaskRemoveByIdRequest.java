package ru.tsc.bagrintsev.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@AllArgsConstructor
public final class TaskRemoveByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

}
