package ru.tsc.bagrintsev.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.client.IDomainEndpointClient;
import ru.tsc.bagrintsev.tm.dto.request.data.*;
import ru.tsc.bagrintsev.tm.dto.response.data.*;

@NoArgsConstructor
public final class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpointClient {

    public DomainEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public BackupLoadResponse loadBackup(@NotNull final BackupLoadRequest request) {
        return call(request, BackupLoadResponse.class);
    }

    @NotNull
    @Override
    public BackupSaveResponse saveBackup(@NotNull final BackupSaveRequest request) {
        return call(request, BackupSaveResponse.class);
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadBase64(@NotNull final DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveBase64(@NotNull final DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadBinary(@NotNull final DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveBinary(@NotNull final DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    public DataJacksonJsonLoadResponse loadJacksonJson(@NotNull final DataJacksonJsonLoadRequest request) {
        return call(request, DataJacksonJsonLoadResponse.class);
    }

    @NotNull
    @Override
    public DataJacksonJsonSaveResponse saveJacksonJson(@NotNull final DataJacksonJsonSaveRequest request) {
        return call(request, DataJacksonJsonSaveResponse.class);
    }

    @NotNull
    @Override
    public DataJacksonXmlLoadResponse loadJacksonXml(@NotNull final DataJacksonXmlLoadRequest request) {
        return call(request, DataJacksonXmlLoadResponse.class);
    }

    @NotNull
    @Override
    public DataJacksonXmlSaveResponse saveJacksonXml(@NotNull final DataJacksonXmlSaveRequest request) {
        return call(request, DataJacksonXmlSaveResponse.class);
    }

    @NotNull
    @Override
    public DataJacksonYamlLoadResponse loadJacksonYaml(@NotNull final DataJacksonYamlLoadRequest request) {
        return call(request, DataJacksonYamlLoadResponse.class);
    }

    @NotNull
    @Override
    public DataJacksonYamlSaveResponse saveJacksonYaml(@NotNull final DataJacksonYamlSaveRequest request) {
        return call(request, DataJacksonYamlSaveResponse.class);
    }

    @NotNull
    @Override
    public DataJaxbJsonLoadResponse loadJaxbJson(@NotNull final DataJaxbJsonLoadRequest request) {
        return call(request, DataJaxbJsonLoadResponse.class);
    }

    @NotNull
    @Override
    public DataJaxbJsonSaveResponse saveJaxbJson(@NotNull final DataJaxbJsonSaveRequest request) {
        return call(request, DataJaxbJsonSaveResponse.class);
    }

    @NotNull
    @Override
    public DataJaxbXmlLoadResponse loadJaxbXml(@NotNull final DataJaxbXmlLoadRequest request) {
        return call(request, DataJaxbXmlLoadResponse.class);
    }

    @NotNull
    @Override
    public DataJaxbXmlSaveResponse saveJaxbXml(@NotNull final DataJaxbXmlSaveRequest request) {
        return call(request, DataJaxbXmlSaveResponse.class);
    }

}
