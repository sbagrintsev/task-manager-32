package ru.tsc.bagrintsev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;

import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String PASSWORD_HASH_ITERATIONS_DEFAULT = "65536";

    @NotNull
    public static final String PASSWORD_HASH_ITERATIONS_KEY = "passwordHash.iterations";

    @NotNull
    public static final String PASSWORD_HASH_KEY_LENGTH_DEFAULT = "128";

    @NotNull
    public static final String PASSWORD_HASH_KEY_LENGTH_KEY = "passwordHash.keyLength";

    @NotNull
    public static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    public static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    public final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        @NotNull final String envKey = getEnvKey(key);
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return "1.31.test";
//        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return "testName";
//        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return "testEmail";
//        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public Integer getPasswordHashIterations() {
        @NotNull final String value = getStringValue(PASSWORD_HASH_ITERATIONS_KEY, PASSWORD_HASH_ITERATIONS_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public Integer getPasswordHashKeyLength() {
        @NotNull final String value = getStringValue(PASSWORD_HASH_KEY_LENGTH_KEY, PASSWORD_HASH_KEY_LENGTH_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String value = getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
        return Integer.parseInt(value);
    }

}
