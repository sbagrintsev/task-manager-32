package ru.tsc.bagrintsev.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.enumerated.Role;

public class DisconnectCommand extends AbstractCommand {

    public static final String NAME = "disconnect";

    @Override
    @SneakyThrows
    public void execute() {
        try {
            getConnectionEndpoint().disconnect();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getShortName() {
        return "";
    }

    @Override
    public @NotNull String getDescription() {
        return "Disconnect from server";
    }

    @Override
    public @NotNull Role[] getRoles() {
        return new Role[0];
    }

}
