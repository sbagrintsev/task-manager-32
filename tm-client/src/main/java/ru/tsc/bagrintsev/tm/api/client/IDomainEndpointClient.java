package ru.tsc.bagrintsev.tm.api.client;

import ru.tsc.bagrintsev.tm.api.endpoint.IDomainEndpoint;

public interface IDomainEndpointClient extends IEndpointClient, IDomainEndpoint {
}
