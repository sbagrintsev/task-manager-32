package ru.tsc.bagrintsev.tm.api.client;

import ru.tsc.bagrintsev.tm.api.endpoint.IUserEndpoint;

public interface IUserEndpointClient extends IEndpointClient, IUserEndpoint {
}
