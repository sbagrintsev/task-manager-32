package ru.tsc.bagrintsev.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.client.IAuthEndpointClient;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignInRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignOutRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserViewProfileRequest;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignInResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignOutResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserViewProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @Override
    public @NotNull UserSignInResponse signIn(@NotNull UserSignInRequest request) {
        return call(request, UserSignInResponse.class);
    }

    @Override
    public @NotNull UserSignOutResponse signOut(@NotNull UserSignOutRequest request) {
        return call(request, UserSignOutResponse.class);
    }

    @Override
    public @NotNull UserViewProfileResponse viewProfile(@NotNull UserViewProfileRequest request) {
        return call(request, UserViewProfileResponse.class);
    }

}
