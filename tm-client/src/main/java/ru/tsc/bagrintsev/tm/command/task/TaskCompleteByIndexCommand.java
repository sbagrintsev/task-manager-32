package ru.tsc.bagrintsev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskChangeStatusByIndexRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

public class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.INDEX);
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskEndpoint().changeTaskStatusByIndex(new TaskChangeStatusByIndexRequest(index, "COMPLETED"));
    }

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete task by index.";
    }

}
