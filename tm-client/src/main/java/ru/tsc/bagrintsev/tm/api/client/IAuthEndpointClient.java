package ru.tsc.bagrintsev.tm.api.client;

import ru.tsc.bagrintsev.tm.api.endpoint.IAuthEndpoint;

public interface IAuthEndpointClient extends IEndpointClient, IAuthEndpoint {
}
