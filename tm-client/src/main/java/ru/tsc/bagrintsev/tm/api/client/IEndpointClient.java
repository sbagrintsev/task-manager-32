package ru.tsc.bagrintsev.tm.api.client;

import org.jetbrains.annotations.NotNull;

import java.net.Socket;

public interface IEndpointClient {

    @NotNull
    Socket connect();

    @NotNull
    Socket disconnect();

    @NotNull
    Socket getSocket();

    void setSocket(@NotNull Socket socket);

}
