package ru.tsc.bagrintsev.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.enumerated.Role;

import java.net.Socket;

public class ConnectCommand extends AbstractCommand {

    public static final String NAME = "connect";

    @Override
    @SneakyThrows
    public void execute() {
        try {
            @NotNull final Socket socket = getConnectionEndpoint().connect();
            getServiceLocator().getAuthEndpoint().setSocket(socket);
            getServiceLocator().getProjectEndpoint().setSocket(socket);
            getServiceLocator().getDomainEndpoint().setSocket(socket);
            getServiceLocator().getSystemEndpoint().setSocket(socket);
            getServiceLocator().getTaskEndpoint().setSocket(socket);
            getServiceLocator().getUserEndpoint().setSocket(socket);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }

    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getShortName() {
        return "";
    }

    @Override
    public @NotNull String getDescription() {
        return "Connect to server";
    }

    @Override
    public @NotNull Role[] getRoles() {
        return new Role[0];
    }
}
