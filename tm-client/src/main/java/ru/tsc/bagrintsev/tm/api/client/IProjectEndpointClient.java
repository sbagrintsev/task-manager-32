package ru.tsc.bagrintsev.tm.api.client;

import ru.tsc.bagrintsev.tm.api.endpoint.IProjectEndpoint;

public interface IProjectEndpointClient extends IEndpointClient, IProjectEndpoint {
}
