package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJacksonJsonLoadRequest;

public final class DataJacksonJsonLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json";

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        getDomainEndpoint().loadJacksonJson(new DataJacksonJsonLoadRequest());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load current application state from json file";
    }

}
