package ru.tsc.bagrintsev.tm.api.client;

import ru.tsc.bagrintsev.tm.api.endpoint.ITaskEndpoint;

public interface ITaskEndpointClient extends IEndpointClient, ITaskEndpoint {
}
