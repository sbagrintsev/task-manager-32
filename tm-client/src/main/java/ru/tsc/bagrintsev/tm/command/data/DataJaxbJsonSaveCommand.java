package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJaxbJsonSaveRequest;

public final class DataJaxbJsonSaveCommand extends AbstractDataCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        getDomainEndpoint().saveJaxbJson(new DataJaxbJsonSaveRequest());
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-jaxb-json";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save current application state in json file";
    }

}
