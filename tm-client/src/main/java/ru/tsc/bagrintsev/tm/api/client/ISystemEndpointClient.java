package ru.tsc.bagrintsev.tm.api.client;

import ru.tsc.bagrintsev.tm.api.endpoint.ISystemEndpoint;

public interface ISystemEndpointClient extends IEndpointClient, ISystemEndpoint {
}
