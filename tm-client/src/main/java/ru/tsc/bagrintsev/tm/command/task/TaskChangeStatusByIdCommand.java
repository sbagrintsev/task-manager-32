package ru.tsc.bagrintsev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.ID);
        @Nullable final String id = TerminalUtil.nextLine();
        showParameterInfo(EntityField.STATUS);
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        getTaskEndpoint().changeTaskStatusById(new TaskChangeStatusByIdRequest(id, statusValue));
    }

    @NotNull
    @Override
    public String getName() {
        return "task-change-status-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change task status by id.";
    }

}
