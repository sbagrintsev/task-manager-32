package ru.tsc.bagrintsev.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

public class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.ID);
        @Nullable final String id = TerminalUtil.nextLine();
        getProjectEndpoint().changeProjectStatusById(new ProjectChangeStatusByIdRequest(id, "COMPLETED"));
    }

    @NotNull
    @Override
    public String getName() {
        return "project-complete-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete project by id.";
    }

}
