package ru.tsc.bagrintsev.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.user.UserLockRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        @NotNull final String login = TerminalUtil.nextLine();
        getUserEndpoint().lock(new UserLockRequest(login));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getName() {
        return "user-lock";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Admin functional: lock user.";
    }

}
