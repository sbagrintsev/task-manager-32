package ru.tsc.bagrintsev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.client.IAuthEndpointClient;
import ru.tsc.bagrintsev.tm.api.endpoint.IUserEndpoint;
import ru.tsc.bagrintsev.tm.client.AuthEndpointClient;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.system.ServiceLocatorNotInitializedException;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    public IUserEndpoint getUserEndpoint() throws AbstractException {
        if (serviceLocator == null) throw new ServiceLocatorNotInitializedException();
        return serviceLocator.getUserEndpoint();
    }

    @NotNull
    public IAuthEndpointClient getAuthEndpoint() throws AbstractException {
        if (serviceLocator == null) throw new ServiceLocatorNotInitializedException();
        return serviceLocator.getAuthEndpoint();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

    @NotNull
    @Override
    public String getShortName() {
        return "";
    }

}
