package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJaxbXmlSaveRequest;

public final class DataJaxbXmlSaveCommand extends AbstractDataCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        getDomainEndpoint().saveJaxbXml(new DataJaxbXmlSaveRequest());
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-jaxb-xml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save current application state in xml file";
    }

}
