package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.client.*;
import ru.tsc.bagrintsev.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IEndpointClient getConnectionEndpoint();

    @NotNull
    IAuthEndpointClient getAuthEndpoint();

    @NotNull
    IDomainEndpointClient getDomainEndpoint();

    @NotNull
    IProjectEndpointClient getProjectEndpoint();

    @NotNull
    ISystemEndpointClient getSystemEndpoint();

    @NotNull
    ITaskEndpointClient getTaskEndpoint();

    @NotNull
    IUserEndpointClient getUserEndpoint();

}
