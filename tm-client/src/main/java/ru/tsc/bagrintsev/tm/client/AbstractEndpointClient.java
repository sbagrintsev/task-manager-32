package ru.tsc.bagrintsev.tm.client;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.client.IEndpointClient;
import ru.tsc.bagrintsev.tm.dto.response.ApplicationErrorResponse;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpointClient implements IEndpointClient {

    @NotNull
    protected String host = "localhost";

    @NotNull
    protected Integer port = 6060;

    @NotNull
    protected Socket socket;

    public AbstractEndpointClient(@NotNull final AbstractEndpointClient client) {
        this.host = client.host;
        this.port = client.port;
        this.socket = client.socket;
    }

    @SneakyThrows
    private OutputStream getOutputStream() {
        return socket.getOutputStream();
    }

    @SneakyThrows
    private InputStream getInputStream() {
        return socket.getInputStream();
    }

    @NotNull
    @SneakyThrows
    private ObjectOutputStream getObjectOutputStream() {
        return new ObjectOutputStream(getOutputStream());
    }

    @SneakyThrows
    private ObjectInputStream getObjectInputStream() {
        return new ObjectInputStream(getInputStream());
    }

    @NotNull
    @SneakyThrows
    protected <T> T call(
            @NotNull final Object data,
            @NotNull final Class<T> clazz
    ) {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof ApplicationErrorResponse) {
            @NotNull final ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    @NotNull
    @SneakyThrows
    public Socket connect() {
        socket = new Socket(host, port);
        return socket;
    }

    @NotNull
    @SneakyThrows
    public Socket disconnect() {
        socket.close();
        return socket;
    }

}
