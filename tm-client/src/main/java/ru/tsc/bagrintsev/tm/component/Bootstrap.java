package ru.tsc.bagrintsev.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.bagrintsev.tm.api.client.*;
import ru.tsc.bagrintsev.tm.api.repository.ICommandRepository;
import ru.tsc.bagrintsev.tm.api.sevice.ICommandService;
import ru.tsc.bagrintsev.tm.api.sevice.ILoggerService;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.client.*;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.command.server.ConnectCommand;
import ru.tsc.bagrintsev.tm.command.server.DisconnectCommand;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.repository.CommandRepository;
import ru.tsc.bagrintsev.tm.service.CommandService;
import ru.tsc.bagrintsev.tm.service.LoggerService;
import ru.tsc.bagrintsev.tm.service.PropertyService;
import ru.tsc.bagrintsev.tm.util.SystemUtil;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IEndpointClient connectionEndpoint = new ConnectionEndpointClient();

    @NotNull
    private final IAuthEndpointClient authEndpoint = new AuthEndpointClient();

    @NotNull
    private final IDomainEndpointClient domainEndpoint = new DomainEndpointClient();

    @NotNull
    private final IProjectEndpointClient projectEndpoint = new ProjectEndpointClient();

    @NotNull
    private final ISystemEndpointClient systemEndpoint = new SystemEndpointClient();

    @NotNull
    private final ITaskEndpointClient taskEndpoint = new TaskEndpointClient();

    @NotNull
    private final IUserEndpointClient userEndpoint = new UserEndpointClient();

    private boolean continueExecute = true;

    {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.bagrintsev.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        classes.forEach(this::registry);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        @NotNull final AbstractCommand command = clazz.getDeclaredConstructor().newInstance();
        registryCommand(command);
    }

    private void connect() {
        processOnTheGo(ConnectCommand.NAME);
    }

    private void disconnect() {
        processOnTheGo(DisconnectCommand.NAME);
    }

    public void run(@Nullable final String[] args) {
        processOnStart(args);
        prepare();
        while (continueExecute) {
            try {
                System.out.println();
                System.out.println("Enter Command:");
                System.out.print(">> ");
                @NotNull final String command = TerminalUtil.nextLine();
                processOnTheGo(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (Exception e) {
                loggerService.error(e);
                e.printStackTrace();
                System.err.println("[FAIL]");
            }
        }
    }

    private void registryCommand(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager-client.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void prepare() {
        initPID();
        loggerService.info("*** Welcome to Task Manager ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
        connect();
    }

    @SneakyThrows
    private void shutdown() {
        loggerService.info("*** Task Manager is shutting down... ***");
        disconnect();
        stopExecute();
    }

    @SneakyThrows
    private void processOnStart(@Nullable final String arg) {
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByShort(arg);
        abstractCommand.execute();
    }

    @SneakyThrows
    private void processOnStart(@Nullable final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            @Nullable final String arg = args[0];
            processOnStart(arg);
            commandService.getCommandByName("exit").execute();
        } catch (AbstractException | GeneralSecurityException e) {
            loggerService.error(e);
        }
    }

    @SneakyThrows
    private void processOnTheGo(@Nullable final String command) {
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        abstractCommand.execute();
    }

    public void stopExecute() {
        continueExecute = false;
    }

}
