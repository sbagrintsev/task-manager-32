package ru.tsc.bagrintsev.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.endpoint.IUserEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.api.sevice.IUserService;
import ru.tsc.bagrintsev.tm.dto.request.user.*;
import ru.tsc.bagrintsev.tm.dto.response.user.*;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.model.User;

public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLockResponse lock(@NotNull final UserLockRequest request) {
        check(request, Role.ADMIN);
        @NotNull final String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUnlockResponse unlock(@NotNull final UserUnlockRequest request) {
        check(request, Role.ADMIN);
        @NotNull final String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRemoveResponse remove(@NotNull final UserRemoveRequest request) {
        check(request, Role.ADMIN);
        @NotNull final String login = request.getLogin();
        getUserService().removeByLogin(login);
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserSignUpResponse signUp(@NotNull final UserSignUpRequest request) {
        @NotNull final String login = request.getLogin();
        @NotNull final String password = request.getPassword();
        @NotNull final Role role = request.getRole();
        @NotNull final String email = request.getEmail();
        @NotNull final User user = getUserService().create(login, password);
        getUserService().setParameter(user, EntityField.EMAIL, email);
        getUserService().setRole(user, role);
        return new UserSignUpResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserChangePasswordResponse changePassword(@NotNull final UserChangePasswordRequest request) {
        check(request, Role.values());
        @NotNull final String userId = request.getUserId();
        @NotNull final String newPassword = request.getNewPassword();
        @NotNull final String oldPassword = request.getOldPassword();
        @NotNull final User user;
        try {
            user = getUserService().setPassword(userId, newPassword, oldPassword);
        } catch (IdIsEmptyException e) {
            throw new UserNotFoundException();
        }
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUpdateProfileResponse updateProfile(@NotNull final UserUpdateProfileRequest request) {
        check(request, Role.values());
        @NotNull final String userId = request.getUserId();
        @NotNull final String firstName = request.getFirstName();
        @NotNull final String middleName = request.getMiddleName();
        @NotNull final String lastName = request.getLastName();
        @NotNull final User user;
        try {
            user = getUserService().updateUser(userId, firstName, lastName, middleName);
        } catch (IdIsEmptyException e) {
            throw new UserNotFoundException();
        }
        return new UserUpdateProfileResponse(user);
    }

}
