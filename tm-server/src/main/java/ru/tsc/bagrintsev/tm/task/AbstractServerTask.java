package ru.tsc.bagrintsev.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.component.Server;

public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected final Server server;

    public AbstractServerTask(@NotNull final Server server) {
        this.server = server;
    }

}
