package ru.tsc.bagrintsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.IDomainService;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.dto.request.data.*;
import ru.tsc.bagrintsev.tm.dto.response.data.*;
import ru.tsc.bagrintsev.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IDomainService getDomainService() {
        return getServiceLocator().getDomainService();
    }

    @NotNull
    @Override
    public BackupLoadResponse loadBackup(@NotNull final BackupLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadBackup();
        return new BackupLoadResponse();
    }

    @NotNull
    @Override
    public BackupSaveResponse saveBackup(@NotNull final BackupSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveBackup();
        return new BackupSaveResponse();
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadBase64(@NotNull final DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveBase64(@NotNull final DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadBinary(@NotNull final DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveBinary(@NotNull final DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    public DataJacksonJsonLoadResponse loadJacksonJson(@NotNull final DataJacksonJsonLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadJacksonJSON();
        return new DataJacksonJsonLoadResponse();
    }

    @NotNull
    @Override
    public DataJacksonJsonSaveResponse saveJacksonJson(@NotNull final DataJacksonJsonSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveJacksonJSON();
        return new DataJacksonJsonSaveResponse();
    }

    @NotNull
    @Override
    public DataJacksonXmlLoadResponse loadJacksonXml(@NotNull final DataJacksonXmlLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadJacksonXML();
        return new DataJacksonXmlLoadResponse();
    }

    @NotNull
    @Override
    public DataJacksonXmlSaveResponse saveJacksonXml(@NotNull final DataJacksonXmlSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveJacksonXML();
        return new DataJacksonXmlSaveResponse();
    }

    @NotNull
    @Override
    public DataJacksonYamlLoadResponse loadJacksonYaml(@NotNull final DataJacksonYamlLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadJacksonYAML();
        return new DataJacksonYamlLoadResponse();
    }

    @NotNull
    @Override
    public DataJacksonYamlSaveResponse saveJacksonYaml(@NotNull final DataJacksonYamlSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveJacksonYAML();
        return new DataJacksonYamlSaveResponse();
    }

    @NotNull
    @Override
    public DataJaxbJsonLoadResponse loadJaxbJson(@NotNull final DataJaxbJsonLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadJaxbJSON();
        return new DataJaxbJsonLoadResponse();
    }

    @NotNull
    @Override
    public DataJaxbJsonSaveResponse saveJaxbJson(@NotNull final DataJaxbJsonSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveJaxbJSON();
        return new DataJaxbJsonSaveResponse();
    }

    @NotNull
    @Override
    public DataJaxbXmlLoadResponse loadJaxbXml(@NotNull final DataJaxbXmlLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadJaxbXML();
        return new DataJaxbXmlLoadResponse();
    }

    @NotNull
    @Override
    public DataJaxbXmlSaveResponse saveJaxbXml(@NotNull final DataJaxbXmlSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveJaxbXML();
        return new DataJaxbXmlSaveResponse();
    }
}
