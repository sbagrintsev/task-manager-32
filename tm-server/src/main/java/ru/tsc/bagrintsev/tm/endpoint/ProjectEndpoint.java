package ru.tsc.bagrintsev.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectService;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.dto.request.project.*;
import ru.tsc.bagrintsev.tm.dto.response.project.*;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.model.Project;

import java.util.List;

public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull final ProjectChangeStatusByIdRequest request) {
        check(request, Role.values());
        @Nullable final String id = request.getId();
        @Nullable final String statusValue = request.getStatusValue();
        @Nullable final Status status = Status.toStatus(statusValue);
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().changeProjectStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull final ProjectChangeStatusByIndexRequest request) {
        check(request, Role.values());
        @Nullable final Integer index = request.getIndex();
        @Nullable final String statusValue = request.getStatusValue();
        @Nullable final Status status = Status.toStatus(statusValue);
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectClearResponse clearProject(@NotNull final ProjectClearRequest request) {
        check(request, Role.values());
        @Nullable final String userId = request.getUserId();
        getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectCreateResponse createProject(@NotNull final ProjectCreateRequest request) {
        check(request, Role.values());
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectListResponse listProject(@NotNull final ProjectListRequest request) {
        check(request, Role.values());
        @Nullable final String sortValue = request.getSortValue();
        @Nullable final String userId = request.getUserId();
        @Nullable final Sort sort = Sort.toSort(sortValue);
        @Nullable final List<Project> projects = getProjectService().findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectRemoveByIdResponse removeProjectById(@NotNull final ProjectRemoveByIdRequest request) {
        check(request, Role.values());
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().findOneById(userId, id);
        getServiceLocator().getProjectTaskService().removeProjectById(userId, project.getId());
        return new ProjectRemoveByIdResponse(project);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        check(request, Role.values());
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().findOneByIndex(userId, index);
        serviceLocator.getProjectTaskService().removeProjectById(userId, project.getId());
        return new ProjectRemoveByIndexResponse(project);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectShowByIdResponse showProjectById(@NotNull final ProjectShowByIdRequest request) {
        check(request, Role.values());
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().findOneById(userId, id);
        return new ProjectShowByIdResponse(project);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectShowByIndexResponse showProjectByIndex(@NotNull final ProjectShowByIndexRequest request) {
        check(request, Role.values());
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().findOneByIndex(userId, index);
        return new ProjectShowByIndexResponse(project);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectUpdateByIdResponse updateProjectById(@NotNull final ProjectUpdateByIdRequest request) {
        check(request, Role.values());
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        check(request, Role.values());
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }
}
