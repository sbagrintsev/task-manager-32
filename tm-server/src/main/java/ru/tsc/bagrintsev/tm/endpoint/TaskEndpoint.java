package ru.tsc.bagrintsev.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.api.sevice.ITaskService;
import ru.tsc.bagrintsev.tm.dto.request.task.*;
import ru.tsc.bagrintsev.tm.dto.response.task.*;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;

public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        check(request, Role.values());
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        getServiceLocator().getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull final TaskChangeStatusByIdRequest request) {
        check(request, Role.values());
        @Nullable final String id = request.getTaskId();
        @Nullable final String statusValue = request.getStatusValue();
        @Nullable final Status status = Status.toStatus(statusValue);
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().changeTaskStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull final TaskChangeStatusByIndexRequest request) {
        check(request, Role.values());
        @Nullable final Integer index = request.getIndex();
        @Nullable final String statusValue = request.getStatusValue();
        @Nullable final Status status = Status.toStatus(statusValue);
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().changeTaskStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskClearResponse clearTask(@NotNull final TaskClearRequest request) {
        check(request, Role.values());
        @Nullable final String userId = request.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCreateResponse createTask(@NotNull final TaskCreateRequest request) {
        check(request, Role.values());
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskListByProjectIdResponse listTaskByProjectId(@NotNull final TaskListByProjectIdRequest request) {
        check(request, Role.values());
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String userId = request.getUserId();
        @Nullable final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        return new TaskListByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskListResponse listTask(@NotNull final TaskListRequest request) {
        check(request, Role.values());
        @Nullable final String sortValue = request.getSortValue();
        @Nullable final String userId = request.getUserId();
        @Nullable final Sort sort = Sort.toSort(sortValue);
        @Nullable final List<Task> tasks = getTaskService().findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIdResponse removeTaskById(@NotNull final TaskRemoveByIdRequest request) {
        check(request, Role.values());
        @Nullable final String id = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().findOneById(userId, id);
        getTaskService().removeById(userId, task.getId());
        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIndexResponse removeTaskByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        check(request, Role.values());
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().findOneByIndex(userId, index);
        getTaskService().removeById(userId, task.getId());
        return new TaskRemoveByIndexResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByIdResponse showTaskById(@NotNull final TaskShowByIdRequest request) {
        check(request, Role.values());
        @Nullable final String id = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().findOneById(userId, id);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByIndexResponse showTaskByIndex(@NotNull final TaskShowByIndexRequest request) {
        check(request, Role.values());
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().findOneByIndex(userId, index);
        return new TaskShowByIndexResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        check(request, Role.values());
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        getServiceLocator().getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIdResponse updateTaskById(@NotNull final TaskUpdateByIdRequest request) {
        check(request, Role.values());
        @Nullable final String id = request.getTaskId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIndexResponse updateTaskByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        check(request, Role.values());
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }
}
