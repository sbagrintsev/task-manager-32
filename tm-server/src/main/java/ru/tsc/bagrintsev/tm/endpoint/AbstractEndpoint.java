package ru.tsc.bagrintsev.tm.endpoint;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.user.AccessDeniedException;
import ru.tsc.bagrintsev.tm.exception.user.PermissionDeniedException;
import ru.tsc.bagrintsev.tm.model.User;

import java.util.Arrays;

public class AbstractEndpoint {

    @Getter
    @NotNull
    final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @SneakyThrows
    protected void check(@NotNull final AbstractUserRequest request) {
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
    }

    @SneakyThrows
    protected void check(@NotNull final AbstractUserRequest request,
                         @NotNull final Role[] roles
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        if (userId == null) throw new AccessDeniedException();
        @NotNull final User user = getServiceLocator().getUserService().findOneById(userId);
        @NotNull final Role userRole = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(userRole);
        if (!hasRole) throw new PermissionDeniedException();
    }

    @SneakyThrows
    protected void check(@NotNull final AbstractUserRequest request,
                         @NotNull final Role role
    ) {
        check(request, new Role[]{role});
    }
}
