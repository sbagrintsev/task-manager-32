package ru.tsc.bagrintsev.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.endpoint.*;
import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.repository.IUserRepository;
import ru.tsc.bagrintsev.tm.api.sevice.*;
import ru.tsc.bagrintsev.tm.dto.request.data.*;
import ru.tsc.bagrintsev.tm.dto.request.project.*;
import ru.tsc.bagrintsev.tm.dto.request.system.AboutRequest;
import ru.tsc.bagrintsev.tm.dto.request.system.InfoRequest;
import ru.tsc.bagrintsev.tm.dto.request.system.VersionRequest;
import ru.tsc.bagrintsev.tm.dto.request.task.*;
import ru.tsc.bagrintsev.tm.dto.request.user.*;
import ru.tsc.bagrintsev.tm.endpoint.*;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.TaskRepository;
import ru.tsc.bagrintsev.tm.repository.UserRepository;
import ru.tsc.bagrintsev.tm.service.*;
import ru.tsc.bagrintsev.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, this);
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(AboutRequest.class, systemEndpoint::getAbout);
        server.registry(InfoRequest.class, systemEndpoint::getInfo);
        server.registry(VersionRequest.class, systemEndpoint::getVersion);

        server.registry(BackupLoadRequest.class, domainEndpoint::loadBackup);
        server.registry(BackupSaveRequest.class, domainEndpoint::saveBackup);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveBase64);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::loadBinary);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::saveBinary);
        server.registry(DataJacksonJsonLoadRequest.class, domainEndpoint::loadJacksonJson);
        server.registry(DataJacksonJsonSaveRequest.class, domainEndpoint::saveJacksonJson);
        server.registry(DataJacksonXmlLoadRequest.class, domainEndpoint::loadJacksonXml);
        server.registry(DataJacksonXmlSaveRequest.class, domainEndpoint::saveJacksonXml);
        server.registry(DataJacksonYamlLoadRequest.class, domainEndpoint::loadJacksonYaml);
        server.registry(DataJacksonYamlSaveRequest.class, domainEndpoint::saveJacksonYaml);
        server.registry(DataJaxbJsonLoadRequest.class, domainEndpoint::loadJaxbJson);
        server.registry(DataJaxbJsonSaveRequest.class, domainEndpoint::saveJaxbJson);
        server.registry(DataJaxbXmlLoadRequest.class, domainEndpoint::loadJaxbXml);
        server.registry(DataJaxbXmlSaveRequest.class, domainEndpoint::saveJaxbXml);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectListRequest.class, projectEndpoint::listProject);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeProjectById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::showProjectById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::showProjectByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindTaskToProject);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskListByProjectIdRequest.class, taskEndpoint::listTaskByProjectId);
        server.registry(TaskListRequest.class, taskEndpoint::listTask);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::showTaskById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::showTaskByIndex);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindTaskFromProject);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changePassword);
        server.registry(UserLockRequest.class, userEndpoint::lock);
        server.registry(UserRemoveRequest.class, userEndpoint::remove);
        server.registry(UserSignUpRequest.class, userEndpoint::signUp);
        server.registry(UserUnlockRequest.class, userEndpoint::unlock);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateProfile);
    }

    public void run() {
        initPID();
        initUsers();
        initDemoData();
        loggerService.info("*** Task Manager Server Started ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
        backup.start();
        server.start();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        userRepository.findAll()
                .forEach(user -> {
                    try {
                        initUserData(user.getLogin());
                    } catch (AbstractException e) {
                        System.err.println("Data initialization error...");
                        loggerService.error(e);
                    }
                });
    }

    private void initUserData(String login) throws AbstractException {
        @NotNull final String userId = userRepository.findByLogin(login).getId();
        taskService.create(userId, String.format("%s first task", login), String.format("%s simple description", login));
        taskService.create(userId, String.format("%s second task", login), String.format("%s simple description", login));
        taskService.create(userId, String.format("%s third task", login), String.format("%s simple description", login));
        taskService.create(userId, String.format("%s fourth task", login), String.format("%s simple description", login));
        projectService.create(userId, String.format("%s first project", login), String.format("%s simple description", login));
        projectService.create(userId, String.format("%s second project", login), String.format("%s simple description", login));
        projectService.create(userId, String.format("%s third project", login), String.format("%s simple description", login));
        projectService.create(userId, String.format("%s fourth project", login), String.format("%s simple description", login));
    }

    private void shutdown() {
        loggerService.info("*** Task Manager Server Stopped ***");
        backup.stop();
        server.stop();
    }

    private void initUsers() {
        try {
            userService.create("test", "test").setEmail("test@test.ru");
            userService.create("admin", "admin").setRole(Role.ADMIN);
        } catch (GeneralSecurityException | AbstractException e) {
            System.err.println("User initialization error...");
            loggerService.error(e);
        }
    }

}
