package ru.tsc.bagrintsev.tm.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.sevice.IAuthService;
import ru.tsc.bagrintsev.tm.api.sevice.IUserService;
import ru.tsc.bagrintsev.tm.component.Server;
import ru.tsc.bagrintsev.tm.dto.request.AbstractRequest;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignInRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignOutRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserViewProfileRequest;
import ru.tsc.bagrintsev.tm.dto.response.AbstractResponse;
import ru.tsc.bagrintsev.tm.dto.response.ApplicationErrorResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignInResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignOutResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserViewProfileResponse;
import ru.tsc.bagrintsev.tm.model.User;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public final class ServerRequestTask extends AbstractServerSocketTask {

    @Nullable
    private AbstractRequest request;

    @Nullable
    private AbstractResponse response;

    public ServerRequestTask(
            @NotNull final Server server,
            @NotNull final Socket socket
    ) {
        super(server, socket);
    }


    public ServerRequestTask(
            @NotNull final Server server,
            @NotNull final Socket socket,
            @Nullable final String userId
    ) {
        super(server, socket, userId);
    }

    @Override
    @SneakyThrows
    public void run() {
        processInput();
        processUserId();
        processSignIn();
        processProfile();
        processSignOut();
        processOperation();
        processOutput();
    }

    @SneakyThrows
    private void processInput() {
        @NotNull final InputStream inputStream = socket.getInputStream();
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Object object = objectInputStream.readObject();
        request = (AbstractRequest) object;
    }

    private void processUserId() {
        if (!(request instanceof AbstractUserRequest)) return;
        @NotNull final AbstractUserRequest abstractUserRequest = (AbstractUserRequest) request;
        abstractUserRequest.setUserId(userId);
    }

    private void processSignIn() {
        if (response != null) return;
        if (!(request instanceof UserSignInRequest)) return;
        try {
            @NotNull final UserSignInRequest userSignInRequest = (UserSignInRequest) request;
            @Nullable final String login = userSignInRequest.getLogin();
            @Nullable final String password = userSignInRequest.getPassword();
            @NotNull final IAuthService authService = server.getServiceLocator().getAuthService();
            @NotNull final User user = authService.checkUser(login, password);
            userId = user.getId();
            response = new UserSignInResponse();
        } catch (@NotNull final Exception e) {
            response = new UserSignInResponse(e);
        }
    }

    private void processProfile() {
        if (response != null) return;
        if (!(request instanceof UserViewProfileRequest)) return;
        if (userId == null) {
            response = new UserViewProfileResponse();
            return;
        }
        try {
            @NotNull final IUserService userService = server.getServiceLocator().getUserService();
            @Nullable final User user = userService.findOneById(userId);
            response = new UserViewProfileResponse(user);
        } catch (Exception e) {
            response = new UserViewProfileResponse(e);
        }
    }

    private void processSignOut() {
        if (response != null) return;
        if (!(request instanceof UserSignOutRequest)) return;
        userId = null;
        response = new UserSignOutResponse();
    }

    private void processOperation() {
        if (response != null) return;
        if (request == null) return;
        try {
            @NotNull final Object result = server.call(request);
            response = (AbstractResponse) result;
        } catch (Exception e) {
            response = new ApplicationErrorResponse(e);
        }
    }

    @SneakyThrows
    private void processOutput() {
        @NotNull final OutputStream outputStream = socket.getOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(response);
        server.submit(new ServerRequestTask(server, socket, userId));
    }

}
