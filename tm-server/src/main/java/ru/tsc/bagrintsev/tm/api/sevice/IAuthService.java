package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.User;

import java.security.GeneralSecurityException;

public interface IAuthService extends Checkable {

    User checkUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException, GeneralSecurityException;

    void checkRoles(@NotNull final String userId,
                    @NotNull final Role[] roles
    ) throws AbstractException;

}
